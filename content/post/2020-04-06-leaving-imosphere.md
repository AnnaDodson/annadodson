---
title: "New Job Alert"
date: "2020-04-06"
tags: ["lifestyle"]
toc: false
description: "With a heavy heart, I'm sad to say I'm leaving Imosphere but I'm very excited to be joining the DVSA"
slug: "new-job"
---

This is my last working week at Imosphere, I'm very sad to be leaving my amazing colleagues and working on such worthwhile products but the time has come for a new challenge and I'm very excited to be joining the {{% link href="https://dvsadigital.blog.gov.uk/" name="Driver & Vehicle Standards Agency" %}} (DVSA) in the Digital Services and Technology team.

I'm a huge advocate for Open Source, a fan of all things DevOps and I love using Linux. Working in a Windows ecosystem on proprietary software was starting to feel a bit round peg square hole plus being a small company, the opportunities to grow weren't always there. I happened to see a post in the {{% link href="https://www.technottingham.com/" name="Tech Nottingham" %}} Slack Jobs channel at an organization that I'd always thought sounded like a great place to work, so on a bit of a whim and quite late in the day I decided to apply and see what would happen.

Thankfully I liked them and they liked me so I gladly accepted the offer and with a heavy heart I faced the fact that I would be leaving Imosphere.

## COVID-19

When I applied, COVID-19 was on the news but not quite front page yet and when I accepted, it seemed like people would be working from home for a few weeks - I never imagined we'd be in lockdown and I'd need to get my company laptop delivered to my house! I was of course worried about leaving a stable job to start a new contract in the middle of a global pandemic and I had to be sensible and consider the potential financial ramifications if the worst happened. I've known my new manager, {{% link href="https://dvsadigital.blog.gov.uk/2018/01/02/what-its-like-being-a-senior-digital-developer-at-dvsa/" name="Shaun Hare" %}} for a few years and he's a great person (those of you who know Shaun will be nodding in agreement here) so we talked through my concerns and he was a huge help in discussing my options and reassuring me.

One of my close friends who I have huge respect for also recently accepted a position at a new company and we talked it through for some extra reassurance and guidance. Having a strong network has been a huge help for me. I was nervous about going into a team with so few women again but knowing other women who work there that I was able to talk to definitely set my mind at ease.

## My new role

The DVSA set out a data and technology strategy (2017 to 2022), {{% link href="https://www.gov.uk/government/publications/dvsa-strategy-2017-to-2022" name="you can read it here" %}}, the focus points I'm particularly interested in are accessibility, availability, open data and agile practices. I love that they're working with {{% link href="https://github.com/dvsa" name="Open Source" %}} and {{% link href="https://dvsadigital.blog.gov.uk/2018/01/05/opening-up-our-mot-history-data/" name="open data " %}} because the people are paying for and using this service - they deserve to be able to see and access the technology they're paying for.

The tech stack is a PHP (mostly 7) and AWS (Amazon Web Services). I've not done a lot of PHP before despite being a regular at {{% link href="https://phpminds.org/" name="PHPMinds" %}} meetups for a few years now! I'm looking forward to learning the language and getting more involved in the incredible PHP community.

## Last thoughts

The last few weeks have been challenging and worrying, fortunately I have a few days off next week to recharge my batteries. Starting a new job remotely during a global pandemic and country wide lockdown is definitely a bit daunting but I've always loved a challenge!
