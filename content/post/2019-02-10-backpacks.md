---
title: "Buying a practical and stylish backpack was a lot harder than I thought it would be"
date: "2019-02-10"
tags: ["lifestyle"]
description: "This is my new lifestyle post! I had a couple of conferences and events in January lined up where I'd be carrying my laptop and other stuff so I needed a backpack to meet all my needs. It was a lot harder than I thought it would be"
slug: "backpacks"
---

## Buying a backpack - how hard can it be?

I use a backpack everyday for work and at lots of different events and conferences. My normal one was lacking in the features department so I'd planned for a post Christmas upgrade.

I thought this would be a simple, easy and a fun thing to do. I ended up spending _hours_ looking at bags online, reading reviews, watching promo videos on youtube (which is an awesome resource if you're buying something online btw). In total I ordered 7 backpacks before finding **the one**.

Lots of people helped me along my journey of backpack buying so I thought I'd document what I found and maybe save someone hours of amazon trawling and doing returns.

## Use case

Going to work, travelling to and attending awesome tech events where I need to take my laptop and a load of other stuff.

## Requirements

Separate laptop compartment - if your bag's full of stuff and you need to get your laptop out, this can be a pain and awkward when all your stuff falls out all over the floor.

Decent size - not giant, I don't want to look like an armadillo.

Rain resistant - I'm not going camping in Antarctica but if it rains, the last thing I want is a soggy laptop.

Looks cool - I gotta look good!

## User research

I checked out backpacks at the tech meetups I go to. If I knew the person I asked them but sometimes I just made notes from afar. If you saw me avidly staring at your stuff in January - I wasn't planning to mug you, I was admiring your backpack.

I asked friends for recommendations, Carol, Helen, Jess, Nina - you all have great backpacks! One friend recommended the brand Dakine which was a great help. Carol also helpfully pointed out that she loves her yellow backpack but it gets dirty on the bottom easily.

## Spiking out different options

  {{< backpack >}}

## And the winner is...

After _all_ those I narrowed down what I wanted to most: separate laptop section, cool lining ideally but definitely a light colour so I can see into the bag, a good every day size - not massive. None of the above met all of the perfectly reasonable and achievable demands.


  {{< backpackwinner >}}

<a href="https://www.ogio.com/womens/soho-womens-laptop-backpack/spr4704960.html" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i> Bag on the Ogio website</a>

