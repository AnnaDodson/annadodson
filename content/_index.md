---
date: "2017-06-26"
title: "Anna Dodson"
---

I'm a Lead Developer at {{% link href="https://dvsadigital.blog.gov.uk/" name="Driver and Vehicle Standards Agency (DVSA)" %}}, working on the internal payments API.

In my spare time I love getting involved in the tech community, I'm on the organising team for {{% link href="https://www.technottingham.com/" name="Tech Nottingham" %}} and {{% link href="https://www.technottingham.com/wit-notts" name="Women in Tech" %}} and I enjoy supporting and mentoring people in the community with their careers or getting started with public speaking.

I live with my partner {{% link href="https://jvt.me" name="Jamie Tanna" %}} and our little horror of a cat, {{% link href="https://twitter.com/anna_hax/status/1030573279831105536" name="Morphs" %}} and our latest addition, our new puppy {{% link href="https://twitter.com/anna_hax/status/1526619204899725314" name="Cookie" %}}.

I like Linux - primarily {{% link href="https://www.ubuntu.com/" name="Ubuntu" %}}, Free and Open Source Software, messing around with my {{% link href="https://gitlab.com/AnnaDodson/configs" name="configs" %}} and automating stuff, especially our home!

My blog is my place for documenting stuff I've learned and talking about things I care about. Sometimes I even publish the posts I write!

{{% link href="mailto:website@annadodson.co.uk" icon="fa-envelope-o" name="Get in touch if you want to say hi" %}}!
