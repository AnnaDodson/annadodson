---
title: "Installing AWS SAM with pip on Ubuntu"
date: "2020-10-04"
tags: ["aws", "blogumentation"]
toc: false
description: "I just found out the recommended way to install AWS SAM on Linux is to use Homebrew!?! Homebrew is a fantastic package manager for Mac. This blog explains how to install sam with pip."
slug: "install-aws-sam-with-pip"
---

I just found out the recommended way to install AWS SAM on Linux is to use Homebrew!?! Homebrew is a fantastic package manager for Mac. This blog explains how to install SAM with pip on Ubuntu.

You'll need Python 3 installed for the latest SAM cli. Check you have have it installed and what it's called. It's installed on Ubuntu by default in all the latest versions. On my machine it's called `python3`.

Install pip for python3 if it's not already installed:

```
$ sudo apt-get install python3-pip
```

And then to install AWS SAM cli:

```
pip3 install --user aws-sam-cli
```

Note, the `--user` is optional and is often the default anyway. It installs to the Python user install directory for your platform.

You should now have SAM cli installed ready for action! Check it's installed by running `sam --version`.