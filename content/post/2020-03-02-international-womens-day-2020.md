---
title: "Building Inclusive Workplaces - part two"
date: "2020-03-02"
tags: ["diversity", "wit"]
description: "I'm celebrating International Women's Day by talking about women's issues that aren't just women's issues. Enjoy!"
slug: "international-womens-day"
---

## It's International Women's Day on Sunday!

[Last year]({{< ref 2019-03-08-international-womens-day >}}) I wrote about helping women in the workplace and this year I want to carry on with that theme.

I love International Women's Day, I am in awe of all the incredible women I am lucky enough to know, who are doing amazing things. They inspire me to be better, challenge my ways of thinking, and push me to do more because as far as we have come - there's so much more to do.

I want to share some of their stories and some of my own, in the hope that by next year these things will be happening much less often.

Again I'm going to ask you to imagine...

1. You're in a meeting and you're not sure about something. It doesn't make sense to you so you interject and ask the speaker to explain their previous point. He doesn't hear you over someone else starting to ask a question and doesn't respond. You feel a bit silly that he didn't hear so you decide to keep quiet. You're sure it must have been obvious as everyone else understood it - it must just be you that didn't get it. It's probably best that no one heard you. You don't ask questions in meetings again.

2. Everyone's quietly working away at their desks, headphones on and busy with what they're doing. You need to confirm something with the team as it's a team decision so you open up slack and post a message in your team chat. You detail the points you need feedback on and explain the situation. You know everyone's busy so you jump onto something else whilst waiting for some replies. About an hour goes by and then your manager posts in the chat, there's some new work coming our way and he wants to share what he's learned. Everyone jumps on the chat and ask questions about it. By morning the next day no one has responded to your question and you don't know how to proceed. You're embarrassed to say you're being ignored during stand up.

3. There's a bug been raised in the newest build, it's pretty serious and is impacting the rest of the team. You were the last one to work in that area of the code base. Your team lead alerts you to the issue "Looks like your last change has broken this - can you look at it straight away?". Of course, you jump on it. The other team members start helping by reviewing your latest changes and highlighting all the things you should have done differently and what they think is causing the problem. You try and reply to them answering all their questions, whilst also trying to investigate the issue. It seems like your responses don't convince them, so they ask even more questions. Your team lead doesn't have time to wait so decides to revert the change without speaking to you first. That doesn't fix it though, as it turns out it was a change further up that impacted it. No one apologises. You go to the ladies and have a little cry. This is the third time this has happened in the last year.

If you're not taken seriously, or respected as much as your peers, it can get really hard to "be good at your job".

If you read this and it sounds a bit familiar - let's grab a coffee together! Maybe you recognise both points of view in these stories, it's hard to fight against what you've been led to believe your whole life. Behaviours can change for better or worse - let's make it better.

Happy International Women's Day! :yellow_heart:

[If you're interested, you can read part one here.]({{< ref 2019-03-08-international-womens-day >}})
