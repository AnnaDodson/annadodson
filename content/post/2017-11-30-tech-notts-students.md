---
title: "Tech Nottingham Student Event"
date: "2017-11-30"
tags: ["talks", "community"]
description: "I did a talk for the Tech Nottingham students about my career journey so far"
slug: "tech-notts-students"
---

<h3>Tech Nottingham student outreach event with Experian</h3>

Tech Nottingham ran an evening event at Experian's offices for students in Nottingham, there were talks by ex-students and local businesses to talk about working life, tech in general, getting a job in tech and why the tech scene in Nottingham is the best! I gave a short talk on my experience of being a student, going into the workplace, going back to uni and now working as a developer in Nottingham. I wanted to convey that job hunting can be tough and share my experiences and the ups and downs I've had.

<!-- more -->


When I started back at uni doing my masters I joined HackSoc and loved it so much! Going to meetups after uni was a natural step and Tech Nottingham definitely has the same community spirit as the society did. I've learnt so much from attending meetups, not just the varied topics covered but by meeting new and interesting people.


My journey into working in tech has been eventful and anything but a straight road. Perhaps because I haven't come from a conventional CS background, I often find I don't relate to some of the advice and case studies from uni career events or usual graduate job boards, so I wanted to share my story and hope to help others who were like me; from a different background or people who just plain old lack confidence and like to hear someone else has gone through struggles and come out the other end. Like anyone doing any kind of talk, I wanted to help and inspire.


<div style="margin: 30px 0;">
  <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/presentation/d/e/2PACX-1vSson6StktmbeJXqRLRZw3NPSUcmj5Gp8ll6U9V77m6essxf9t4RludKV9qeDjP07WVEhdNZb9Vs7Qe/embed?start=false&loop=false&delayms=3000" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
  </div>
</div>

<h2>Learning is Fun</h2>
Getting an introduction to a subject (the programming language GO, for example, or using AWS CLI) by someone who is knowledgable _and_ passionate about the subject is a fantastic way to learn about something new and stay current with new technologies. Being able to say to my manager or team why I want to use a particular tool over another one or tell them how other companies are solving their problems is a great feeling. I don't use Docker at work but when it started gaining popularity I knew what it was, what problems it was trying to solve, good use cases for it and how to get a basic container set up on my laptop, all in one evening!

<h2>Life Skills</h2>
Meetups are also a fantastic place to meet like-minded people. It's super easy to make small talk with anyone there because you are both pretty much guaranteed to have things in common to chat about. Social interactions like small talk and networking are a very valuable skill and can definitely be the deciding factor between two similar skilled applicants at an interview. These soft skills are very important and can be difficult, some people can find social interactions very daunting, I definitely do. Which is why having safe community spaces just like university societies really help. Tech Nottingham and all the community meetups ran by volunteers is an amazing resource not just for tech knowledge but a fantastic community to be part of. All the people there want to be there and that generally means they're pretty awesome and very approachable.

<h2>Q&A</h2>

At the end of my talk I was asked some really good questions. Here's a couple along with my answers:

<span style="font-size:26px">Q</span> Did you think that companies wouldn't hire you because you didn't have a Computer Science degree?<br>
<span style="font-size:26px">A</span> I definitely used to think that. I didn't speak to some companies at university career events because I was convinced they only wanted students doing CS undergrads and wouldn't be interested in me. Of course they _want_ people with CS degrees but more importantly they want people who are enthusiastic about tech, any decent company where I want to work wouldn't overlook any candidate who showed enthusiasm and passion because they had a different degree.

<span style="font-size:26px">Q</span> Did you find it hard getting interviews and offers?<br>
<span style="font-size:26px">A</span> I had help writing my CV because I found it hard, again down to low self confidence. I'm genuinely passionate about development and tech and all the hackathons and metups I attend help prove this and is a great thing to talk about. I wasn't offered a job at a company I really wanted to work at and I found that really really hard. After I didn't get that position, though, I applied elsewhere which is where I now work and it's actually a much better role for me and a lot more inline with what I want to be doing! It definitely didn't feel good at the time but now, I'm so happy I didn't get that job!

