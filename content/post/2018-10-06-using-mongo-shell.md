---
title: "Quick Getting Started MongoDB Commands"
date: "2018-10-06"
slug: "using-mongo-shell"
---

### New to MongoDB

I recently needed to interact with a Mongo Database for the first time and I found myself trying to remember these shell commands quite often so I've documented them here for future reference.

<!-- more -->


Getting started with the shell. To connect a terminal to the currently running mongo db:

```
$ mongo --port <port-number>
```

In my case, that was 3001. If you don't know what the port number is, you can run this command on a system terminal:

```
$ sudo lsof -iTCP -sTCP:LISTEN | grep mongo
```
Found from [StackOverflow](https://stackoverflow.com/a/9348481).

Then to list all the databases to knew which one to connect to:

```
$ show dbs
```

To use one that's listed to run commands against

```
$ use <database-name>
```

To find out the collections, run:

```
$ show collections
```

To count how many,

```
$ db.<collection-name>.count()
```



### Further reading:

Full docs [docs.mongodb.com/manual/mongo/](https://docs.mongodb.com/manual/mongo/).

The CLI quick reference is also really handy [docs.mongodb.com/manual/reference/mongo-shell/](https://docs.mongodb.com/manual/reference/mongo-shell/)
