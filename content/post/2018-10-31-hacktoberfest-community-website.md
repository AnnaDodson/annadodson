---
layout: post
title: "Hacktoberfest Community Website"
date: "2018-10-31"
tags: [hacktoberfest, community]
toc: false
description: "This year I had a go at being a maintainer for Hacktoberfest by creating a website for anyone in the Tech Nottingham community to contribute too and especially looking to help first timers contribute to open source"
slug: "hacktoberfest-community-website"
---

# We built this for Hacktoberfest

I'm not going to say much about it here, you're going to have to go and check it out yourself to see what it was all about.

## The why
:memo: {{% link href="https://webuiltthisforhacktoberfest.netlify.com/contributing/community/blog/2018/10/02/annadodson-blog-post.html" name="webuiltthisforhacktoberfest.netlify.com" %}}

## The who

:nerd_face: {{% link href="https://github.com/AnnaDodson/hacktoberfest-website/pulls?q=is%3Apr+is%3Aclosed" name="https://github.com/AnnaDodson/hacktoberfest-website/pulls" %}}

## The end

:hourglass: I had such a good time being a maintainer and I learned a lot! It's hard to keep a project going and let others have their views and opinions when they're all different. I was so happy a few first timers contributed and I really liked the diverse range of posts from all different people. I'm looking forward to what next year brings :jack_o_lantern:
