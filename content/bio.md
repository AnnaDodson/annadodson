---
date: "2014-04-09"
title: "Bio"
---

{{< figure
src="/profile-pic.png" 
alt="Photo of Anna Dodson at Women in Tech event"
>}}

Anna is a Software Engineer at the [Driver and Vehicle Standards Agency](https://dvsadigital.blog.gov.uk/) (DVSA) working on the internal payments API. She also helps organise [Tech Nottingham](https://www.technottingham.com) and [Women in Tech](https://www.technottingham.com/wit-notts) as well as advocating for Free and Open Source Software and inclusivity in tech. Collector of pretty keyboards and cute stickers.