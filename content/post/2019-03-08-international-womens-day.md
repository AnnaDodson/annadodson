---
title: "Building Inclusive Workplaces"
date: "2019-03-08"
tags: ["diversity", "wit"]
description: "I'm celebrating International Women's Day by talking about womens issues that aren't just womens issues. Enjoy!"
slug: "international-womens-day"
---

# It's International Women's Day Today!

I love it when I get asked about inclusivity and improving diversity in the tech industry, it's a subject I could talk about all day long! I also love easy, practical and achievable solutions.

There's one simple change a lot of companies could make that I believe would make a huge difference to women.

Imagine….

 - You have a meeting in the conference room at the other end of the building, the meeting overruns, some people want to catch up after, there are some important points you want to raise. But you don't have any sanitary products with you… so you need to go all the way back to your desk, get your products before heading to the loos, which are probably next to the conference rooms cos that's how the world works! Gah.

 - How about you have some potential clients coming in for a meeting? It's a mixed group and it's important you make a good impression so you encourage them to leave their coats and bags in the meeting room whilst you have a tea break and posh biccies in the fancy part of the office. But now you've separated a woman who may be on her period from her bag. She probably doesn't know how to get back to the room without asking and then she has to go and find the loos. What a lovely nice relaxing tea break.

 - Or what if you're just at work as a normal day without a care in the world and you get up - you've had a few coffees, so you walk to the loo and then halfway there, you remember it's your favourite time of the month (yay!) so you go back to your desk and slyly get your stuff together and stuff it up your sleeve, in your jeans pocket or carry it openly and proudly (respect) through the heavily male-dominated open plan office….

I've experienced all these scenarios and more. Having products in the loos make a huge difference to the everyday lives of women at work.
 
If your company invests in the wellbeing of their staff, this could be something they could do. If your company is already doing this, you're awesome!! Please advertise this loud and proud to help other companies see the light.

People who read this that don't have periods and still empathise, thank you.

If you want to hear more on this subject, here's a great 5 minute video <a href="https://twitter.com/short_louise" target="_blank">@short_louise</a> recommended:

<div class="video-container">
    <iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/zrhPoizfhxw" frameborder="0" allowfullscreen ></iframe>
</div>

And you can also join the <a href="http://www.tampon.club/" target="_blank">tampon club</a> too! Thanks <a href="https://twitter.com/Loftio" target="_blank">Lex</a> for the tip!

Happy International Women's Day! :yellow_heart:
