---
title:  "A Talk on Ransomware for Linney Labs"
date:   "2017-07-23"
tags: ["talks"]
description: "I spoke at Linney on Ransomware and especially WannaCry"
slug: "ransomware"
---

<h3>$300 to Get Your Files Back</h3>

I did a talk explaining Ransomware to the rest of the company recently. The talk covered what Ransomware is and why it's become such a successful malware, looking in particular at the WannaCry outbreak and an overview of public/private key encryption.

<!-- more -->

---

As part of the Digital Team at Linney we get asked to present to the company on topics related to our work to share knowledge across the business. These sessions run once every two months and topics range from UX design, current projects, AI and the most recent on web security. I presented Ransomware during the session and explained what it is and how it works, focusing on WannaCrypt that made headlines earlier this year.

<div style="margin: 30px 0;">
  <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/presentation/d/e/2PACX-1vQtokccEZsFr-7AvThqIu3AvQgnH7_haZw786Y4CadRdMNbTfYtcdSLIOi2GCF_8JhnUpXq7GgDKirq/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
  </div>
</div>

During the WannaCryptor outbreak, there was a lot of press coverage about the NHS being under 'attack' and I wanted to make it clear this wasn't a targeted attack and explain how important keeping software updated is. In the NHSs case, keeping software updated is not an easy task and shows how dependencies can lead to problems in the future.

I also covered the NSA's involvement and wanted to discuss their role in the Eternal Blue exploit which was used to successfully propagate the malware. The NSA may defend their right not to disclose vulnerabilities in order to 'fight terrorism' but their 'backdoor' led to malicious actors using the exploit to their advantage which caused disruption and distress to many working in and the patients of the NHS.

