Personal Website
=====

Recently moved from Jekyll to Hugo.

Template: https://github.com/AnnaDodson/cupper-hugo-theme.

An accessibility-friendly Hugo theme, ported from the original Cupper project.

License: MIT License.

## Getting Started

Clone the repo and then to get the theme run: 

```
$ git submodule update --init
```

This will clone the themes repo into the theme folder to run locally from.

## To run and build

To build and run locally, first install Hugo extended
** Note the extended is very important for sass support

To run locally:
```
hugo serve
```

To build
```
hugo
```
