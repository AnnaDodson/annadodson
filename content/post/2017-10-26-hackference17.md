---
title: "Hackference 2017"
date: "2017-10-26"
tags: ["hackference", "conference", "hackathon"]
description: "I Went to Hackference Conference and Hackathon in Birmingham"
slug: "hackference17"
---

<h3>Hackference - a Conference and Hack</h3>

I attended [Hackference](https://2017.hackference.co.uk/), a conference for learning and playing with new fun stuff. It's based in Birmingham and is a very community focused event, the conference is for learning and then a 24 hour hackathon is run over the weekend to build something with the new things everyone's learnt and the sponsors APIs.

<!-- more -->

I attended the following talks:

- JavaScript - Browser bits - Ben Foxall
- Infrastructure as Cake - Jamie Tanna
- Hardware Hacking for JavaScript Developers - Tim Perry
- NetDevOps: DevOps in networking - Mircea Ulinic
- Code is not only for computers, but also for humans - Parham Doustdar
- Building a Serverless Data Pipeline - Lorna Mitchell
- JavaScript = The exotic parts: Workers, WebGL and Web Assembly - Martin Splitt
- Privacy could be the next big thing - Stuart Langridge

### JavaScript - Browser bits by Ben Foxall
Great talk by Ben talking through optimising pixel manipulation in the browser. Really interesting learning how to get audio and video in real time within the browser.

Ben did a demo during which he called his mum through the browser using Nexmo, it was very brave and brilliantly executed! Proved the browser was rendering display in direct response to the audio playing

### Infrastructure as Cake by Jamie Tanna
Jamie Tanna explained Configuration Management, why you need it and how Chef helps you do it. I particularly found the testing aspect of the talk thought provoking, having only used bash scripts to configure my server, the idea of testing the provisioning is very interesting. The talk was aimed at enterprise level infrastructure but interesting takeaways for testing and the approach to server management.

I especially loved the glossary for Chef! The talk had lots of references to cookbooks, recipes and some great food puns too. I'm not sure I have a use case for Chef presently but I am tempted to try Ansible as I'm definitely sold on configuration management now. Hopefully I can play with Chef one day in the future.

No cake though. Loses points for not bringing cake.

### Hardware Hacking for JavaScript Developers - Tim Perry
Loved this talk for being the most inspiring and gave me some great ideas for christmas presents! I can't wait to get my dad an Ardunino for checking the soil moiusure levels in the garden from the comfort of his living room.

The presentation started by Tim writing a few lines of code in JavaScripts, connecting his laptop to the bluetooth puck and viola, a clicker for the presentation! Great start and everyone was hooked.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">When a speaker writes their own presentation clicker in 30 lines of live code and a <a href="https://twitter.com/hashtag/MetawareC?src=hash&amp;ref_src=twsrc%5Etfw">#MetawareC</a>, programming IS magic <a href="https://twitter.com/pimterry?ref_src=twsrc%5Etfw">@pimterry</a> <a href="https://twitter.com/hashtag/hackference?src=hash&amp;ref_src=twsrc%5Etfw">#hackference</a> <a href="https://t.co/1M8Nv36Omc">pic.twitter.com/1M8Nv36Omc</a></p>&mdash; Bevis in TheOldSmoke (@bevishalperry) <a href="https://twitter.com/bevishalperry/status/921332442920833025?ref_src=twsrc%5Etfw">October 20, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Then followed a whistle stop tour of applications and hardware for inspiration and a few quick demos of some nice interesting applications using Ardunios and Rasberry Pi's. He then explained practically how to work on hardware and talked about the OS his company builds, it's based on Linux and uses Docker to deploy your app onto the board, this makes deployments and debugging a lot easier and therefore faster.

I'm sold and now want a load of hardware to hack on.

### NetDevOps: DevOps in networking - Mircea Ulinic
Similar to Jamie Tanna's talk on config management, although as he works at Cloudfares, this talk was aimed at SCALE. He talked about using Salt and how it helped him and his team work Dev Ops and affectively manage the servers. I don't have any current use case for Salt or anywhere near the scale of work but it was interesting hearing about Cloudflare managing their deployments.

### Code is not only for computers, but also for humans - Parham Doustdar
A great talk and really informative from a totally different perspective of development work. This talk really focused on how maintainability should be the number one concern for every decision a developer makes. He made an interesting point that when choosing between languages, writing for maintainability isn't a selling point for any language and Parham really felt strongly that that should be number one.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Communication is important even when writing code 💻 maybe you’ll be the one reading your code in a year <a href="https://twitter.com/PD90?ref_src=twsrc%5Etfw">@pd90</a> <a href="https://twitter.com/hackferencebrum?ref_src=twsrc%5Etfw">@hackferencebrum</a> <a href="https://twitter.com/hashtag/hackference?src=hash&amp;ref_src=twsrc%5Etfw">#hackference</a> <a href="https://t.co/eWplkXuyjW">pic.twitter.com/eWplkXuyjW</a></p>&mdash; Nimvelo (@Nimvelo) <a href="https://twitter.com/Nimvelo/status/921372375047917568?ref_src=twsrc%5Etfw">October 20, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I liked his take on this and it's really made me think about who is going to be maintaining my legacy code (cringe) in the future. Number one takeaway - good decisions shouldn't be left til last or an afterthough, get into the habit of taking the time to do it right as you go along because there's no time given later on to refactor and tidy up.

### Building a Serverless Data Pipeline - Lorna Mitchell
Another great talk! Lorna explained her application, the app goes to Stackoverflow, grabs a load of questions with tags matching her criteria, checks if they're new and not already in the database and if not - add them in and then post a notification into their slack channel using HuBot. The whole thing runs on a cron job every five minutes on IBMs equivalent of Amazons Lambda.

I was really interested in Lorna's approach of offline first too. She talked about her love for CouchDB and it's counterpart PouchDB which uses browser local storage. I've never heard of PouchDB and I don't consider offline first at all. This has got me thinking and I really want to have a play around with some sort of serverless application, I think this is a great way to run simple applications with no need for a server up and running all the time.

### JavaScript - The exotic parts: Workers, WebGL and Web Assembly by Martin Splitt
Similar to the first talk of the day on topic but different approach. Martin was an engaging speaker with a great unique Swiss sense of humour. Good discussion on performance graphic rendering in the browser using Web Workers and webGL. The demo demonstrated the speed of image processing affecting the responsiveness of the UI. WebGL showed an impressive performance boost and a good reason to keep off the main thread!

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">This is speaking my language, JavaScript image analysis and why its so slow without web workers and webGL! <a href="https://twitter.com/g33konaut?ref_src=twsrc%5Etfw">@g33konaut</a> <a href="https://twitter.com/hashtag/hackference?src=hash&amp;ref_src=twsrc%5Etfw">#hackference</a> <a href="https://t.co/SzOyfgSJAy">pic.twitter.com/SzOyfgSJAy</a></p>&mdash; Bevis in TheOldSmoke (@bevishalperry) <a href="https://twitter.com/bevishalperry/status/921395202417463297?ref_src=twsrc%5Etfw">October 20, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Privacy could be the next big thing by Stuart Langridge
Sil closed the day with a great talk on privacy and how data is being collected all the time from everywhere. Companies collecting data about you is "creepy" when they use it to deduce things they weren't told and shouldn't know.

Some interesting points about changing the way peoples data is regulated, how it should have been regulated before it was collected but now it's the norm for companies to collect data of every interaction and presence online.


He made the point that Apple isn't too bad at data farming:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">&quot;Apple don&#39;t need to monetize your data.. Because they&#39;ve monetized your money&quot; <a href="https://twitter.com/sil?ref_src=twsrc%5Etfw">@sil</a> <a href="https://twitter.com/hashtag/hackference?src=hash&amp;ref_src=twsrc%5Etfw">#hackference</a></p>&mdash; Jamie Tanna (@JamieTanna) <a href="https://twitter.com/JamieTanna/status/921408393532858375?ref_src=twsrc%5Etfw">October 20, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## Conclusion
A really educational day, loads of fantastic people and really interesting talks. I learnt a lot and left with some ideas and new things I want to try out. The conference set me up for wanting to hack away over the weekend and build something fun.

Loved the atmosphere of the day and how friendly all the people were and of course, most importantly, some great swag! Hats off to the sponsors and thanks for the socks!
