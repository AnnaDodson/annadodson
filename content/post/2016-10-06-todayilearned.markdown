---
title:  "TIL - Today I Learned"
date:   "2016-10-06"
tags: ["learning"]
description: "I've been doing a lot of driving recently so I've taken the opportunity to listen to some of my favourite tech and programming podcasts."
slug: "todayilearned"
---

<h3>This Week I've Learned.</h3>

I've been doing a lot of driving recently so I've taken the opportunity to listen to some of my favourite tech and programming podcasts.

I've especially been enjoying the <a href="http://www.codingblocks.net/" target="_blank">Coding Blocks</a> podcasts on programming and best practises. The guys are all currently .NET programmers but between them they've pretty much programmed in every language and they're really interesting and funny. Very easy listening for a long drive.

I've just listened to the episodes on Encapsulation and Testing. I'm currently learning Java so the comments on when to use public, private and protected appropriately has really given me some good tips and will definitely make me use my getters and setters properly.

After that episode, I wanted to learn more about <a href="http://www.codingblocks.net/podcast/episode-20-were-testing-your-patience/" targt="blank">unit tests</a>, this episode covered the different approaches of testing and how to think about the best way to unit test when designing and before writing any code. Most helpful to me was to start thinking of unit testing as a development methodology for writing good reusable code and to consider method dependencies.

I don't have a lot of experience running full tests and I'm guilty of using console logs and standard out as my main testing method with they outed as pretty bad practise! I'll definitely be trying to write proper unit tests and look into using some test suites for my next project.

Programming Throwdown did a great episode on <a href="http://www.programmingthrowdown.com/2012/08/episode-17-java.html" target="_blank">Java</a> which covered some really clear explanations on type casting and the history of the language. I also liked hearing about the difference in compliers and how they can be optimised for different processes. 

Right now, I'm currently half way through Coding Blocks <a href="http://www.codingblocks.net/podcast/how-to-write-amazing-functions/" target="_blank">Clean Code</a> review and it's really good, I've been recommended the book before so it's great to hear the presenters walk through it and offer explanations related to their real life experiences. I'm finding it really helpful learning best practises for writing good clean re-usable and readable code, for example thinking of a function as only ever doing _one_ task, even if that's just making a decision and then sending the result to the right function to actually action the task. Using expressive function names too, instead of hunting a function down to see what it's doing - the function name should explicitly say what it's doing and to not be scared of long function names. Interestingly they all agreed that longer function names were preferable to short names and long comments.

I've downloaded about 96 other episodes which all sound really interesting so I hope I have some long car journeys coming up.
