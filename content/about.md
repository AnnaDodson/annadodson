---
date: "2014-04-09"
title: "About"
---

{{< figure
src="/images/logo.svg" 
alt="Photo of Anna Dodson at Women in Tech event"
>}}

Lead Software Engineer at {{% link href="https://dvsadigital.blog.gov.uk/" name="Driver and Vehicle Standards Agency (DVSA)" %}}, currently working on the internal payments API.

The DVSA is part of the Government Digital Services (GDS) which aims to help the government work better for everyone by leading digital transformation. The priority is accessibility for _everyone_.

I haven't always worked in tech, after my bachelors degree, I worked at an apparel company in a project manager role but knew it wasn't where my passion was. I went back to uni to do a Masters in Computer Science at the University of Nottingham, giving up my job and becoming a student again was a big step but I haven't looked back since. I've learned a lot, mainly that most things just take a bit of dedication, patience and passion for learning

During my first term at uni I went to HackNotts, the annual Hack society hackathon and I totally fell in love with hackathons! I have since attended loads more and even won a couple of prizes along the way! From joining HackSoc at uni, I found the Tech Nottingham meetup scene and now regularly attend at least four tech meetups a month.

Whilst learning to code I was always being told about git and how it would revolutionise my programming... turned out that's very true! I <i class="fa fa-heart-o" aria-hidden="true"></i> git. I use GitHub and GitLab a lot and I volunteered at a Hacktoberfest git workshop hosted by HackSoc to help get new students into git and contributing to open source.

I love open source and getting involved with the tech community, I run Ubuntu and enjoy trying to perfect my workspace, becoming a vim wizard and automating my home.

Contributing and volunteering are things I really want to do more of, I volunteered at a code club and loved it. Get in touch if you need help at your event or with a project, I'm always looking for good causes to get involved with.

