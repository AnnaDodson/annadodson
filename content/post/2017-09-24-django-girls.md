---
title: "Django Girls"
date: "2017-09-24"
tags: ["learning", "community"]
description: "Learning Django in Manchester with Django Girls"
slug: "django-girls"
---

<h3>Djagno, girls, cake and MVC at CodeUps workshop</h3>

I started learning programming in Python but I've never used Django before so time to learn! Django Girls is a community led effort to help more women into programming by providing a safe space to learn and a supportive community full of inspiring mentors and helpful resources. 

<!-- more -->

The workshop started Friday night for a social getting everything installed and set up party. Hackathons have taught me that installing and setting up environments can take a long time and get a little bit frustrating especially if you're new to programming.

Saturday was spent working through the example project - to set up a Django blog site and get it deployed live. Mine can be found here: <a href="http://annad.pythonanywhere.com/" target="_blank">annad.pythonanywhere.com</a>. 

I started by setting up a virtual environment to work in, very useful for switching between Python versions and managing the workspace.

Then a whistle stop tour of MVC through Django.

I set my Model as a new Post Object. I added my new Post object `Post` to manage my blog posts. I listed it as a Model and created a class for it with properties of a blog post (author, title, text, publish date) and a method to publish the post.

Once I'd created the model, I needed to inset the entry in my database using the Django migrate manage helper functions. 

First running `makemigrations` with Posts as my new model, so I can create an entry in the database.

Running `migrate` synchronizes the database with the Models. So the changes I made to the Model is updated in the database.

Once I had created my Model and updated my database I needed to create a View to display my blog posts.

Under my new blog directory I created a blog template which I would later extract into a base template and a body content template. 

The interactive Django shell is a great tool for running Django commands like inserting entries into the database, editing them or deleting them. Having a play around on the shell was a great way to test parsing the Posts objects and find ways to display the data.

```
post = Post.objects.filter(published_date__isnull=True).order_by('created_date')
post.get('title' == 'Sample title')
```

I added the queries into my Views to correctly display all the posts that matched the given criteria, firstly to display a list of all blog posts in date order.


<h3>Next steps</h3>
Deciding if I keep my Django site and migrate my current static site to my newly built website. Or does anyone need a website??

 - PR to fix documentation in the Django girls tutorial. Querysets
 - Beef up my website. Extra things. Add draft posts
 - Added security so only I can post. 
 - Change password and username functionality
 - Styling!!
