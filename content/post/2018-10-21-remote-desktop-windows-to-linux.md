---
layout: post
title: "Remote Desktop to Windows from Ubuntu"
date: "2018-10-21"
slug: "remote-desktop-windows-to-linux"
tags: ["ubuntu", "blogumentation"]
---

### Remote Desktop from Ubuntu Using Remmina

I use Windows at work and Ubuntu at home. To work from home I remote desktop onto my work machine using the work VPN. Here are the steps I took to get it set up and working.

<!-- more -->

You'll need your work VPN address and work machine IP and credentials.

I'm running the following versions:
 - Ubuntu 17.10
 - Windows 10
 - Remmina 1.1.2

### To set up the VPN.

Open 'Network Connections' to add the VPN address to your available connections.

Add the VPN address and then click advanced and tick the authentication methods MSCHAP and MSCHAPv2 and also tick Use Point-to-Point encryption (MPPE).

![vpn dialog of the settings][vpn-settings]

Once your VPN is set up, you can connect to it.

From your terminal run 

```
$ nmtui
```

Use your arrow keys and hit enter on 'Activate a connection'. Key down to the end of the list or where it says VPN and hit enter on your VPN name.

![connecting to the vpn with nmtui][network-manager]

Enter your password when it prompts you.

Now you should be connected to your VPN! If not, try running this command and check the information logged out for further information on why you can't connect.

```
$ journalctl -xef
```

### To Remote Desktop with Remina

To remote desktop using Remmina, click to create a new connection and enter your details. I've never had much luck with DNS using the name of the computer so I always enter the IP address.

In Advance > Security, 'Negotiate' should be set by default anyway.

![setting up remmina connection dialog][remmina-login]

Once you've clicked connect you should be able to see your remote desktop!!

### Debugging

If you're still having trouble and can't work out what's wrong, try connecting via the terminal with freerdp2.

If you run the following you should be able to connect or get some helpful errors to figure out where the problem is.

```
$ xfreerdp /u:<your-username-here> /d:<your-domain-here> /v:<your-ip-here>
```

For further command line options, [see here](https://github.com/FreeRDP/FreeRDP/wiki/CommandLineInterface)

I ran the above with the different protocol settings for debugging:
```
$ xfreerdp /u:<your-username-here> /d:<your-domain-here> /v:<your-ip-here> /sec:nla
```

[vpn-settings]: /images/vpn-settings.png
[network-manager]: /images/network-manager.png
[remmina-login]: /images/remmina-login.png
